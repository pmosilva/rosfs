#!/bin/bash
#=============================================================================
# Author: Pedro Marques da Silva <posilva@gmail.com>
# Date: July 2014
# Description:
#	This script installs a named package 
#
#=============================================================================
# get script name
PROGNAME=$( basename $0 )

#-- attention to links 
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#------------------------------------
#
#------------------------------------
usage () {
	echo ""	
	echo "USAGE:"
	echo ""	
	echo "$PROGNAME [package name]"
}

#------------------------------------
# Generic Function to exit
#------------------------------------
die () {
    echo >&2 "$@"
    usage
    exit 1
}
# .. validate inpout 	
[ "$#" -eq 1 ] || die "1 argument required, $# provided"


do_download () {
	pkg_download=$(basename "$pkg_url")
	extension="${pkg_download##*.}"
    pkg_download="${P_NAME}-${pkg_version}.${extension}"   
	# this change dir is necessary because you must set output file name
	if [ ! -e $DOWNLOAD_DIR/$pkg_download ]; then
		
		cd $DOWNLOAD_DIR && \
		wget --directory-prefix=$DOWNLOAD_DIR --output-document=$pkg_download  $pkg_url
    fi
	pkg_download=$DOWNLOAD_DIR/$pkg_download
	export pkg_download
}

do_unpack () {
	tar xf $pkg_download -C $PKG_BUILD_DIR --strip-components=1   && \
    cd $PKG_BUILD_DIR/
}
# ....
do_setup () {

    pkg_setuptools=0
    pkg_catkin=0
    pkg_custom=0
    # this is the default behavieur if no build tool defined 
    if [ -z $pkg_build ]; then
		pkg_catkin=1
    else	
		pkg_build=$(echo $pkg_build | tr '[:upper:]' '[:lower:]')
		if [ $pkg_build == 'setuptools' ];then
			pkg_setuptools=1                                                                                                                                                       
		fi  
		if [ $pkg_build == 'catkin' ]; then
			pkg_catkin=1                                                                                                                                                       
		fi  
		if [ $pkg_build == 'custom' ]; then
			pkg_custom=1                                                                                                                                                       
		fi  
	fi
    export pkg_setuptools
    export pkg_catkin
    export pkg_custom
	
	
}

# Dummy functions 
do_configure () {
    
    if [ "$pkg_catkin" -eq "1" ]; then
		$pkg_cmake_flags cmake . -DCATKIN_ENABLE_TESTING=0 -DCMAKE_INSTALL_PREFIX=$ROS_INSTALL_DIR -DCMAKE_PREFIX_PATH=$ROS_INSTALL_DIR $pkg_cmake_options
    fi    
}

do_build () {
    if [ "$pkg_catkin" -eq "1" ]; then 
		$pkg_cflags $pkg_ldflags make -j1 
    fi
}

do_install () {
    if [ "$pkg_catkin" -eq "1" ]; then
		make install
	else if [ $pkg_setuptools -eq "1" ]; then
			python setup.py  install --prefix=$ROS_INSTALL_DIR --install-lib=$ROS_INSTALL_DIR/lib/python2.7/dist-packages
		fi
	fi   
}
unset pkg_submodules
unset pkg_ldflags
unset pkg_cdflags
unset pkg_cmake_options
unset pkg_cmake_flags
unset PKG_BUILD_DIR

# Set environment directories
PACKAGE_CONFIG=$1

source $PACKAGE_CONFIG

export PKG_BUILD_DIR="$BUILD_DIR/${P_NAME}-${pkg_version}"

mkdir -p $PKG_BUILD_DIR

echo "Installing $P_NAME package "

for action in do_setup do_download do_unpack; do
    	
    eval "$action > $LOG_DIR/${P_NAME}_${action}.log 2>&1"
	if [ $? -eq 0 ]; then
		touch "$PKG_BUILD_DIR/${action}.done"
	else
		die "Failed to execute action: '${action}' in package '$P_NAME' "
	fi
done  

if [ -z "$pkg_submodules" ]; then
    pkg_submodules=( '.' )
fi

n_modules=${#pkg_submodules[*]}

for module in ${pkg_submodules[*]};
do
    cd $PKG_BUILD_DIR/$module
    for action in do_configure do_build do_install; do
    	echo -ne "\tExecute $action in $module..."
	eval "$action > $LOG_DIR/${P_NAME}_${action}_$(basename ${module}).log 2>&1"
	
	if [ $? -eq 0 ]; then
		touch "$PKG_BUILD_DIR/$module/${action}.done"
		echo -e "\t\tDONE!"
	else
		die "Failed to execute action: '${action}' in package '$P_NAME' "
	fi
    done  
done

