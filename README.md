# BUILD ROS from Scratch #

This project aims to create a build tool to install a custom (minimal) ROS distribution in a Linux box

### Usage ###

* git clone https://pmosilva@bitbucket.org/pmosilva/rosfs.git
* cd rosfs

* ./mkdistro.sh hydro ardupilot-ros # first argument is the distro name and rest is each group ou packages to install 
* source hydro/sysroot/opt/ros/hydro/setup.bash
* roscore & # execute a ROS master in background
* rosrun mavlink_ardupilotmega mavlink_ardupilotmega_node