#!/bin/bash
#=============================================================================
# Author: Pedro Marques da Silva <posilva@gmail.com>
# Date: July 2014
# Description:
#	This script creates a custom ros distro installation 
#
#=============================================================================
# get script name
PROGNAME=$( basename $0 )

#-- attention to links 
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#------------------------------------
#
#------------------------------------
usage () {
        echo >&2 "$@"
	echo ""	
	echo "USAGE:"
	echo ""	
	echo "$PROGNAME [ROS distro name] [Package group]..[Package groups]N"
	exit 1
}

#------------------------------------
# Generic Function to exit
#------------------------------------
die () {
    echo >&2 "$@"
    exit 1
}
#------------------------------------
# Get the host Linux Distro
#------------------------------------
get_os () {
	$(command -v lsb_release)
	if [ $? -eq 0 ]; then
		export os_name=$(lsb_release -a 2> /dev/null| grep Distributor | cut -d: -f2 | tr [:upper:] [:lower:] | tr -d '\t' | tr -d ' ' )
	else
		die "Cannot determine the Linux distribution"		
	fi
}

#------------------------------------
# Install dependecies
#------------------------------------
apply_os_dependencies () {

	GROUP_DIR=$1
	if [ -e $GROUP_DIR/${os_name}.deps ]; then
		source $GROUP_DIR/${os_name}.deps
	else
		die "Unknowned OS distro: ${os_name} "		
	fi
}
#------------------------------------
# Install group
#------------------------------------
install_group (){
	
	G=$DISTRO_DIR/$1
	if [ -d $G ]; then
		echo $G
		G_NAME=$(basename $G)
		apply_os_dependencies $G		
		if [ -e $G/packages.list ]; then

			for P_NAME in `cat "$G/packages.list"`; do
			P=$G/$P_NAME.rsp

			if [ -e $P ]; then
				echo "Installing package: " $P_NAME
				export P_NAME
				source $BASE_DIR/install_pkg.sh  $P
			else
				die "Missing package definition file: $P.rsp"
			fi
			done
		else
			die "Missing 'packages.install' file metadata inside '$G_NAME' group"
		fi
	fi

}
# .. validate inpout 	
[ "$#" -gt 1 ] || usage "1 or more argument required, $# provided"

# Set environment directories
export DISTRO_NAME=$1

export BASE_DIR=$SCRIPT_DIR
DISTRO_DIR=$SCRIPT_DIR/distros/${DISTRO_NAME}
[[ -d $DISTRO_DIR ]] || die "Missing distro directory: $DISTRO_DIR"

shift # consume parameter ROS distro name

export DISTRO_DIR
export ROOT_DIR=$SCRIPT_DIR/deploy/${DISTRO_NAME}
export DOWNLOAD_DIR=$ROOT_DIR/downloads
export BUILD_DIR=$ROOT_DIR/build
export SYSROOT_DIR=$ROOT_DIR/sysroot
export LOG_DIR=$ROOT_DIR/logs
export ROS_INSTALL_DIR=$SYSROOT_DIR/opt/ros/$DISTRO_NAME

# Ensure that folders exists
mkdir -p $ROOT_DIR
mkdir -p $DOWNLOAD_DIR
mkdir -p $BUILD_DIR
mkdir -p $SYSROOT_DIR
mkdir -p $LOG_DIR
mkdir -p $ROS_INSTALL_DIR

export CMAKE_INSTALL_PREFIX=$ROS_INSTALL_DIR                                                                                                                                               \
export CMAKE_PREFIX_PATH=$ROS_INSTALL_DIR
                                                                                                                                                  \
# Python ros packages installation setup
if [[ -z "$PYTHONPATH" ]]; then
   PYTHONPATH=$ROS_INSTALL_DIR/lib/python2.7/dist-packages    
else
   PYTHONPATH=$PYTHONPATH:$ROS_INSTALL_DIR/lib/python2.7/dist-packages    
fi

export PYTHONPATH
get_os

install_group core
while (( "$#" )); do
	install_group $1
	shift
done


# Create a ROS file system archive to distribute or install
tar cfz $ROOT_DIR/${DISTRO_NAME}_install.tar.gz -C $SYSROOT_DIR . && echo "Created a archive with all ROS Distro stuff: $ROOT_DIR/${DISTRO_NAME}_install.tar.gz"

